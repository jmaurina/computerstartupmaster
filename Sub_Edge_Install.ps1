﻿##################################################
#                                                #
#              Chromium Edge Install             #
#         ComputerStartup-ChromiumEdge           #
#                                                #
##################################################

param(
    $pScriptFilesRepository = '\\marine-travelift.com\netlogon'
)

    Write-Host "$([environment]::commandline)"

$EdgeReg = Get-ItemProperty -path "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Microsoft Edge" -Name 'DisplayVersion'  -ErrorAction Ignore

If($EdgeReg.DisplayVersion -ne '89.0.774.68'){

    #Unistall previous versions of Google Chrome if found ##########################################################################################################
    Get-Process -name msedge -ErrorAction Ignore | Stop-Process -Force -ErrorAction Ignore

    #Test-Path -Path "$pScriptFilesRepository\Installers\GoogleChromeFull\Chrome_78_0_3904_x64.msi"


    #Install current version of Google Chrome ######################################################################################################################

    Write-Host "$(Get-Date) Installing Edge 89.0.774.63"-ForegroundColor Yellow


        $msifile = "\\marine-travelift.com\NETLOGON\Installers\Edge\MicrosoftEdgeEnterpriseX64_89_0_774_63.msi"
        $msiargs = @(
            "/i"
            "`"$msifile`""
            "/qn"
        )
    start-process msiexec.exe -ArgumentList $msiargs -wait

    #Start-Process -FilePath msiexec.exe -ArgumentList /i, "\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi"<#, /qb #> -Wait
    #Start-Process -FilePath msiexec.exe -ArgumentList "/i, `\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi`, /qb " -Wait


    Write-Host "$(Get-Date) Done installing Edge 89.0.774.63...Moving on" -ForegroundColor Green
}else{
    Write-Host "$(Get-Date) Edge already installed... Moving On"
}