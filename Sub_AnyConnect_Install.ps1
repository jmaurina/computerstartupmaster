﻿Function Log-Output{
    param($pLogfile,
            $pLogMessage = $null)
    
    "$((Get-Date).ToString('yyyy-MM-dd HH:mm:ss.fff'))      $($pLogMessage)" | Out-File -Filepath $pLogfile -Append
    
}

##################################################
#                                                #
#            Cisco AnyConnect Install            #
#                                                #
##################################################
$ScriptName = "Sub_AnyConnect_Install.ps1"
$LogLocation = "C:\Program Files\MTIUpdate"
$LogFile = "$LogLocation\$ScriptName.txt"
$x86UninstallerPath = 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall'

###Uninstall Previous Versions###
$AnyConnect4_4_02034 = Get-ItemProperty -path "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{3AA4F1C9-87A4-437A-B3F4-69100178F709}" -Name 'DisplayVersion'  -ErrorAction Ignore

If($AnyConnect4_4_02034.DisplayVersion -eq "4.5.04029"){
    Write-Host "$(Get-Date) Cisco AnyConnect 4.5.4029 was found...Uninstalling Cisco AnyConnect 4.5.4029" -ForegroundColor Red
#    Log-Output $LogFile "$(Get-Date) Cisco AnyConnect 4.5.4029 was found...Uninstalling Cisco AnyConnect 4.5.4029"
    #$CurrentApp = Get-WmiObject -Class Win32_Product -Filter "IdentifyingNumber = '{3AA4F1C9-87A4-437A-B3F4-69100178F709}'"
    #$CurrentApp.Uninstall()

    $msifile = "\\marine-travelift.com\NETLOGON\Installers\Cisco VPN\AnyConnect_4_5_4029\WinSetup-Release-web-deploy.msi"
    $msiargs = @(
        "/uninstall"
        "`"$msifile`""
        "/qb"
    )
    start-process msiexec.exe -ArgumentList $msiargs -wait}

Write-host "$(Get-Date) Uninstall of previous versions complete... Moving on to installation." -ForegroundColor Green
#Log-Output $LogFile "$(Get-Date) Uninstall of previous versions complete... Moving on to installation."

###Install Current Version###
#Start-Process "C:\TEMP\AnyConnect_4_5_4029.exe" -ArgumentList "/passive" -Wait
#Start-Process -FilePath msiexec.exe -ArgumentList /i, "\\marine-travelift.com\NETLOGON\Installers\Cisco VPN\AnyConnect_4_5_4029\WinSetup-Release-web-deploy.msi", /quiet -Wait

$msifile = "\\marine-travelift.com\NETLOGON\Installers\Cisco VPN\AnyConnect_4_5_4029\WinSetup-Release-web-deploy.msi"
$msiargs = @(
    "/i"
    "`"$msifile`""
    "/qn"
)
start-process msiexec.exe -ArgumentList $msiargs -wait




Write-host "$(Get-Date) Install of Cisco AnyConnect 4.5.4029 version complete." -ForegroundColor Green
#Log-Output $LogFile "$(Get-Date) Install of Cisco AnyConnect 4.5.4029 version complete."
