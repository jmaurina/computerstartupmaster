﻿################################
#                              #
#       Teamviewer Install     #
#                              #
################################

$TeamViewer = "\\marine-travelift.com\netlogon\Installers\TeamViewer"
$TeamViewerPath = "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\TeamViewer"
$UninstallerPath = "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall"

If ($(Test-Path $TeamViewerPath)){
    #Uninstall old version of TeamViewer
    $TeamViewerVersion = Get-ItemProperty -Path $TeamViewerPath -ErrorAction Ignore

    Write-Host "TeamViewer Version 15... Uninstalling" -ForegroundColor Green
    $uninstallTVPath = $(Get-ItemProperty HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\TeamViewer).UninstallString
    Start-Process $uninstallTVPath /S
    Write-Host "TeamViewer Version 15 Done Uninstalling" -ForegroundColor Green
}
# Install Teamviewer
If($(Test-Path C:\Temp) -eq $false){
    New-Item -Path "C:\" -Name "TEMP" -ItemType "Directory"
}
Copy-Item -Path "\\marine-travelift.com\netlogon\Installers\TeamViewer\TeamViewer_15.exe" -Destination "C:\TEMP"

Write-Host "Previous versions uninstalled... installing new version" -ForegroundColor Green
Start-Process -wait "C:\TEMP\TeamViewer_15.exe" /S

Write-Host "Importing settings" -ForegroundColor Green
reg import "$TeamViewer\TeamViewer.reg"

Write-Host "Cleaning Up Files..."
Remove-Item "C:\TEMP\Teamviewer_15.exe"


Write-Host "TeamViewer Installation complete." -ForegroundColor Green




