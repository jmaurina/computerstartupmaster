Function Log-Output{
    param($pLogfile,
            $pLogMessage = $null)
    
    "$((Get-Date).ToString('yyyy-MM-dd HH:mm:ss.fff'))      $($pLogMessage)" | Out-File -Filepath $pLogfile -Append
    
}

Function Execute-SubScript{
    param($pScriptPath,
            $pScriptArguments)
    
    If($pScriptArguments -in ($null,'')){
        &"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -file """$($pScriptPath)"""
    }
    Else{
        &"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -file """$($pScriptPath)""" "$($pScriptArguments)"
    }
}


Function Replace-SubscriptVariables{
    param($String)

    If($String -ne $null){
        $ReturnString = $string
        $ReturnString = $ReturnString.Replace('~~ClientUpdateFolderPath~~',$gXMLConfig.S.Settings.ClientUpdateFolderPath)
        $ReturnString = $ReturnString.Replace('~~ScriptFilesRepository~~',$gXMLConfig.S.Settings.ScriptFilesRepository)
        $ReturnString = $ReturnString.Replace('~~MasterScriptPath~~',$gScriptPath)

        Return $ReturnString
    }
    Else{
        Return ''
    }
}



Import-Module mti.adsi

$gScriptPath = Split-Path -Path $MyInvocation.MyCommand.Path -Parent
$gScriptName = $MyInvocation.MyCommand.Name
$gConfigFile = "$($gScriptPath)\$($gScriptName).xml"
[XML]$gXMLConfig = Get-Content $gConfigFile

New-Item $gXMLConfig.S.Settings.ClientUpdateFolderPath -type directory -force | Out-Null

$gMasterLog = "C:\Program Files\MTIUpdate\$($gScriptName).txt"
$gScriptStartTime = Get-Date


$gReportingAvailable = Test-path $gXMLConfig.S.Settings.ClientReportingPath


Remove-Item -Path $gMasterLog -ErrorAction Ignore
Log-Output $gMasterLog "Computer Startup Script Started!"

Foreach($SubScript in $gXMLConfig.S.SubScripts.SubScript){
    
    $SubScriptPath = Replace-SubscriptVariables $SubScript.ScriptPath
    $SubScriptArguments = Replace-SubscriptVariables $SubScript.ScriptArguments

    $SubScriptName = ($SubScript.ScriptPath).Split('\')[-1]
    $SubScriptLogFileName = "$($SubScriptName)_$($SubScript.ScriptVersion).txt"
    $SubScriptLogFilePath = "$($gXMLConfig.S.Settings.ClientUpdateFolderPath)\$SubScriptLogFileName"

    If($SubScript.Enabled -eq 'True'){ #Is the script enabled?
        
        #Check if Computer is within the deployment group.
        If($SubScript.ADDeploymentGroup -in ('',$null)){ #Group not specified, every computer is in group.
            $InADDeploymentGroup = $true
        }
        Else{ #Group was specified, check if the computer is in the group.
            $ADDeploymentGroupDN = Get-ADSIdn $SubScript.ADDeploymentGroup "group"
            $DeploymentMembers = Get-ADSIMemberCaller $ADDeploymentGroupDN | foreach{($_.split(',')[0]).Replace('CN=','')}
            
            If($DeploymentMembers -contains $env:COMPUTERNAME){     $InADDeploymentGroup = $true     }
            Else{                                                   $InADDeploymentGroup = $false    }
        }
        
        If($InADDeploymentGroup){ #This computer is within the deployment group
            If(Test-Path $SubScriptPath){ #Does the subscript exist
            
                If($SubScript.ExecutionMode -eq 'ALWAYS'){ #Is the script enabled?
                    Remove-Item -LiteralPath $SubScriptLogFilePath -ErrorAction Ignore
                }
                
                If(Test-Path $SubScriptLogFilePath){
                    If((Get-Content $SubScriptLogFilePath | Select-string "SubScript Finished!") -eq $null){
                        Remove-Item -LiteralPath $SubScriptLogFilePath -ErrorAction Ignore
                        Log-Output $gMasterLog "Subscript $($SubScriptName) Did not complete from a previous attempt. The log entry will be deleted and the Subscript will run again"   
                    }
                }

                If((Test-Path $SubScriptLogFilePath) -eq $false){ #If the Subscript log does not exist; run the subscript
                    If (((Get-Date) - $gScriptStartTime).TotalSeconds -lt ($gXMLConfig.S.Settings.ScriptMaxSeconds - $SubScript.EstimatedRuntime)){ #If there is enough time to execute
                        Log-Output $SubScriptLogFilePath "SubScript Started!"
                        Execute-SubScript $SubScriptPath $SubScriptArguments | Out-File -Filepath $SubScriptLogFilePath -Append
                        Log-Output $SubScriptLogFilePath "SubScript Finished!"

                        Log-Output $gMasterLog "Subscript $($SubScriptName) executed."
                    }
                    Else{
                        Log-Output $gMasterLog "Subscript $($SubScriptName) does not have enough time to execute.  Subscript will be skipped until next startup."
                    }
                }
                Else{
                    Log-Output $gMasterLog "Subscript $($SubScriptName) has already been run on this computer.  Subscript will be skipped."
                }
            }
            Else{
                Log-Output $gMasterLog "Missing subscript script file at location: '$($SubScriptPath)'.  Subscript will be skipped until a valid file is found."
            }
        }
        Else{
            Log-Output $gMasterLog "Subscript $($SubScriptName) requires computers to be in the ADDeploymentGroup of $($SubScript.ADDeploymentGroup).  This computer is not part of the group so the subscript will be skipped.  Valid members of the group are $(Foreach($Memeber in $DeploymentMembers){"$($Memeber),"}).  Add the computer to the group and restart."
        }
    }
    Else{
        Log-Output $gMasterLog "Subscript $($SubScriptName) is disabled in the config file '$($gConfigFile)'.  Subscript will be skipped until it is re-enabled."
    }

    If($SubScript.CleanupOldVersions -eq 'True'){
        $FilesToCleanup = Get-ChildItem -LiteralPath $gXMLConfig.S.Settings.ClientUpdateFolderPath -Filter "$SubScriptName*" | Where {$_.FullName -ne $SubScriptLogFilePath}
        Foreach($FileToCleanup in $FilesToCleanup){
            Remove-Item -LiteralPath $FileToCleanup.FullName -ErrorAction Ignore
        }
    }

    If($gReportingAvailable){
        $ReportingByComputer = "$($gXMLConfig.S.Settings.ClientReportingPath)\ReportingByComputer\$($env:ComputerName)"
        $ReportingByUpdate = "$($gXMLConfig.S.Settings.ClientReportingPath)\ReportingByUpdate\$($SubScriptName)\$($SubScript.ScriptVersion)"

        New-Item $ReportingByComputer -type directory -force | Out-Null
        New-Item $ReportingByUpdate -type directory -force | Out-Null

        If(Test-path $SubScriptLogFilePath){
            Copy-Item $SubScriptLogFilePath "$($ReportingByComputer)\$($SubScriptLogFileName)" -ErrorAction Ignore -Force
            Copy-Item $SubScriptLogFilePath "$($ReportingByUpdate)\$($env:ComputerName).txt" -ErrorAction Ignore -Force
        }
    }
}

Log-Output $gMasterLog "Computer Startup Script Finished!"



#Log Scripting Errors
If($gReportingAvailable){
    If ($error) {
        $ScriptMessage = "$gScriptName completed with the following scripting error(s) on $($env:COMPUTERNAME):`r`n`r`n"

        Foreach($err in $error){       
            $ScriptMessage += "`r`nException: $($err.Exception)"
            $ScriptMessage += "`r`nTargetObject: $($err.TargetObject)"
            $ScriptMessage += "`r`nCategoryInfo: $($err.CategoryInfo)"
            $ScriptMessage += "`r`nFullyQualifiedErrorId: $($err.FullyQualifiedErrorId)"
            $ScriptMessage += "`r`nErrorDetails: $($err.ErrorDetails)"
            $ScriptMessage += "`r`nInvocationInfo: $($err.InvocationInfo)"
            $ScriptMessage += "`r`nScriptStackTrace: $($err.ScriptStackTrace)"
            $ScriptMessage += "`r`nPipelineIterationInfo: $($err.PipelineIterationInfo)"
            $ScriptMessage += "`r`nPSMessageDetails: $($err.PSMessageDetails)"
            $ScriptMessage += "`r`n`r`n--------------------------------------`r`n`r`n"
        }
        
        $error.Clear()

        $ErrorReportingPath = "$($gXMLConfig.S.Settings.ClientReportingPath)\ScriptingErrors"
        New-Item $ErrorReportingPath -type directory -force | Out-Null

        $ScriptMessage | Out-File -Filepath "$($ErrorReportingPath)\$($env:ComputerName)_$((Get-Date).ToString('yyyyMMddHHmmssfff')).txt" -Append
    }
}