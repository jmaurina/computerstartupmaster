param(
    $pScriptFilesRepository = '\\marine-travelift.com\netlogon'
)


Write-Host "$([environment]::commandline)"

#Paths to distribute the modules to.
[ARRAY]$LocalModuleRootPaths =@()
$LocalModuleRootPaths += 'C:\Windows\System32\WindowsPowerShell\v1.0\Modules'
$LocalModuleRootPaths += 'C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules'


$ModuleRepo = Join-path $pScriptFilesRepository 'MISC\Powershell_Modules'
$ModuleRepoPaths = Get-ChildItem -LiteralPath $ModuleRepo | Where-Object {$_.PSIsContainer -eq $true}
Foreach($ModuleRepoPath in $ModuleRepoPaths){

    $ModuleName = $ModuleRepoPath.Name

    #Copy Everything Back
    If($ModuleRepoPath.Name.Substring(0,1) -eq '-'){
        
         #This is a remove only, so we need to remove the minus sign
        $ModuleName = $ModuleName.Substring(1,($ModuleName.Length - 1))

        Foreach($LocalModuleRootPath in $LocalModuleRootPaths){
            
            $LocalModulePath = Join-path $LocalModuleRootPath $ModuleName

            If(Test-Path $LocalModulePath){
                If( (Get-Item $LocalModulePath).Attributes -match 'ReparsePoint' ){
                    Write-host "Local Module Path '$($LocalModulePath) is a Symbolic Link." -ForegroundColor Yellow
                }
                Else{
                    Remove-Item -LiteralPath $LocalModulePath -Recurse -Force
                }
            }

        }

        Write-host "Module Removed: $($ModuleName)" -ForegroundColor Red
    }
    ElseIf($ModuleRepoPath.Name.Substring(0,1) -ne '-'){ #Copy\Sync
        
        Foreach($LocalModuleRootPath in $LocalModuleRootPaths){
            
            $LocalModulePath = Join-path $LocalModuleRootPath $ModuleName

            If( (Get-Item $LocalModulePath).Attributes -match 'ReparsePoint' ){
                Write-host "Local Module Path '$($LocalModulePath) is a Symbolic Link." -ForegroundColor Yellow
            }
            Else{
                #Converted to using robocopy since it is much faster with the MIR option.  It also allows to update files while programs running  No redundant remove/copy
                Robocopy "$($ModuleRepoPath.FullName)" "$($LocalModulePath)" /MIR /NP /NJH /NDL /R:5 /W:2
            }
        }

        Write-host "Module Updated: $($ModuleName)" -ForegroundColor Green
    }

} #Foreach Module