﻿##################################################
#                                                #
#          Google Chrome Full Install            #
#         ComputerStartup-ChromeUpdate           #
#                                                #
##################################################

param(
    $pScriptFilesRepository = '\\marine-travelift.com\netlogon'
)

    Write-Host "$([environment]::commandline)"


#Unistall previous versions of Google Chrome if found ##########################################################################################################
Get-Process -name chrome -ErrorAction Ignore | Stop-Process -Force -ErrorAction Ignore
$app = Get-WmiObject -Class Win32_Product -Filter "Name = 'Google Chrome'"

If($app -ne $null){
    $app.Uninstall()
}


#Test-Path -Path "$pScriptFilesRepository\Installers\GoogleChromeFull\Chrome_78_0_3904_x64.msi"


#Install current version of Google Chrome ######################################################################################################################

Write-Host "$(Get-Date) Installing Google Chrome 89.0.4389.90"-ForegroundColor Yellow


    $msifile = "\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi"
    $msiargs = @(
        "/i"
        "`"$msifile`""
        "/qn"
    )
start-process msiexec.exe -ArgumentList $msiargs -wait

#Start-Process -FilePath msiexec.exe -ArgumentList /i, "\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi"<#, /qb #> -Wait
#Start-Process -FilePath msiexec.exe -ArgumentList "/i, `\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi`, /qb " -Wait


Write-Host "$(Get-Date) Done installing Google Chrome 89.0.4389.90...Moving on" -ForegroundColor Green
