﻿# Update these variables as needed
$CID = 'BA257591E1EE4C26B5AF00CC67B84E19-24'
$SensorShare = '\\marine-travelift.com\NETLOGON\Installers\Crowdstrike\WindowsSensor.MaverickGyr.exe'

# The sensor is copied to the following directory
$SensorLocal = 'C:\Temp\WindowsSensor.exe'

# Create a TEMP directory if one does not already exist
if (!(Test-Path -Path 'C:\Temp' -ErrorAction SilentlyContinue)) {
    Write-Host "Creating some temp directories."
    New-Item -ItemType Directory -Path 'C:\Temp' -Force

}
# Now copy the sensor installer if the share is available
if (Test-Path -Path $SensorShare) {

    Copy-Item -Path $SensorShare -Destination $SensorLocal -Force

}
# Now check to see if the service is already present and if so, don't bother running installer.
if (!(Get-Service -Name 'CSFalconService' -ErrorAction SilentlyContinue)) {
    Write-Host "CrowdStrike is not running. Lets install that."
#    & $SensorLocal /install /quiet /norestart CID=$CID
    Start-Process $SensorLocal -ArgumentList "/install", "/quiet", "/norestart", "CID=BA257591E1EE4C26B5AF00CC67B84E19-24" -Wait

}
#clean up temp file
Write-Host "Cleaning up temp directories."
Remove-Item C:\TEMP -Recurse -Force

