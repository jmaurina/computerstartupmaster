﻿#Confirm Crowdstrike Install

$CSFA = Get-WmiObject -Class Win32_Product -Filter "Name = 'CrowdStrike Firmware Analysis'"
$CSDC = Get-WmiObject -Class Win32_Product -Filter "Name = 'CrowdStrike Device Control'"
$CSSP = Get-WmiObject -Class Win32_Product -Filter "Name = 'CrowdStrike Sensor Platform'"

If($CSFA -ne $null){
    Write-Host "Crowdstrike Firmware Analysis Found on $env:COMPUTERNAME"
}else{
    Write-Host "Crowdstrike Firmware Analysis Not Found on $env:COMPUTERNAME"
}

If($CSDC -ne $null){
    Write-Host "Crowdstrike Device Control Found on $env:COMPUTERNAME"
}else{
    Write-Host "Crowdstrike Device Control Not Found on $env:COMPUTERNAME"
}

If($CSSP -ne $null){
    Write-Host "Crowdstrike Sensor Platform Found on $env:COMPUTERNAME"
}else{
    Write-Host "Crowdstrike Sensor Platform Not Found on $env:COMPUTERNAMECr"
}
