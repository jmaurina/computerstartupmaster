﻿##################################################
#                                                #
#       MailStore Outlook Addin Install          #
#         ComputerStartup-ChromeUpdate           #
#                                                #
##################################################

param(
    $pScriptFilesRepository = '\\marine-travelift.com\netlogon'
)

    Write-Host "$([environment]::commandline)"


#Unistall previous versions of Google Chrome if found ##########################################################################################################
Get-Process -name chrome -ErrorAction Ignore | Stop-Process -Force -ErrorAction Ignore
$app = Get-WmiObject -Class Win32_Product -Filter "Name like 'mailstore%'"

If($app -ne $null){
    Write-Host "Uninstalling old version of MailStore Addin..."
    $app.Uninstall()
}


#Test-Path -Path "\\marine-travelift.com\NETLOGON\Installers\MailStore_Addin\MailStoreOutlookAddin_13_0_5_20118.msi"


#Install current version of Google Chrome ######################################################################################################################

Write-Host "$(Get-Date) Installing MailStore Outlook Addin 12.1.3.14781"-ForegroundColor Yellow

    Get-Process Outlook | Stop-Process -Force

    $msifile = "\\marine-travelift.com\NETLOGON\Installers\MailStore_Addin\MailStoreOutlookAddin_12_1_3_14781.msi"
    $msiargs = @(
        "/i"
        "`"$msifile`""
        "/qn"
    )
start-process msiexec.exe -ArgumentList $msiargs -wait

#Start-Process -FilePath msiexec.exe -ArgumentList /i, "\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi"<#, /qb #> -Wait
#Start-Process -FilePath msiexec.exe -ArgumentList "/i, `\\marine-travelift.com\NETLOGON\Installers\Google Chrome\Chrome_89_0_4389_90_x64.msi`, /qb " -Wait

Start-Process outlook

Write-Host "$(Get-Date) Done installing MailStore Outlook Addin 12.1.3.14781...Moving on" -ForegroundColor Green
